" -----
" Thomas's (@tteoh) nvim config
" -----

" -----
" Plugin manager: vim-plug
" -----

call plug#begin('~/.vim/plugged')

Plug 'jalvesaq/Nvim-R'                  " Nvim-R: working with R
Plug 'lervag/vimtex'                    " vimtex: LaTeX files
Plug 'sickill/vim-monokai'              " vim-monokai: Monokai colorscheme

call plug#end()


" -----
" Colour settings
" -----

set t_Co=256                            " Set colours
colorscheme monokai                     " Set colourscheme

set cursorline                          " Make the current line highlighted
" hi CursorLine term=bold cterm=bold guibg=Grey40

" -----
" Specific formatting preferences
" -----

set showmatch                           " Show matching brackets.
set showcmd                             " Show (partial) command in status line.
set showmode                            " Show current mode.
set ruler                               " Show the line and column numbers of the cursor.
set number                              " Show the line numbers on the left side.
set formatoptions+=o                    " Continue comment marker in new lines.
set textwidth=0                         " Hard-wrap long lines as you type them.
set expandtab                           " Insert spaces when TAB is pressed.
set tabstop=2                           " Render TABs using this many spaces.
set shiftwidth=2                        " Indentation amount for < and > commands.
set mouse=a                             " Enable mouse
set noerrorbells                        " No beeps.
set modeline                            " Enable modeline.
set linespace=0                         " Set line-spacing to minimum.
set nojoinspaces                        " Prevents inserting two spaces after punctuation on a join (J)

set splitbelow                          " Horizontal split below current.
set splitright                          " Vertical split to right of current.

set hlsearch                            " Highlight search results.
set ignorecase                          " Make searching case insensitive
set smartcase                           " ... unless the query has capital letters.
set incsearch                           " Incremental search.
set gdefault                            " Use 'g' flag by default with :s/foo/bar/.
set magic                               " Use 'magic' patterns (extended regular expressions).


" -----
" vim-tex settings
" -----

let g:tex_flavor = 'latex'
" let g:vimtex_view_method = "zathura"
let g:vimtex_view_use_temp_files = 1
let g:vimtex_motion_matchparen = 0
let g:vimtex_indent_enabled = 0


" -----
" Nvim-R settings
" -----

" Automatically start R (alternatively use \rf)
" autocmd FileType r if string(g:SendCmdToR) == "function('SendCmdToR_fake')" | call StartR("R") | endif
" autocmd FileType rmd if string(g:SendCmdToR) == "function('SendCmdToR_fake')" | call StartR("R") | endif
" Automatically quit R when quitting vim (alternatively use \rq)
autocmd VimLeave * if exists("g:SendCmdToR") && string(g:SendCmdToR) != "function('SendCmdToR_fake')" | call RQuit("nosave") | endif
" syntax on
" filetype plugin indent on
let R_assign = 0                       " Disables underscore being a shortcut for <-
" let R_pdfviewer = "zathura"            " Set PDF viewer
let R_openpdf = 0                      " Disable automatic opening of pdf
let R_args_in_stline = 1               " Arguments in status line


" R output is highlighted with current colorscheme
let g:rout_follow_colorscheme = 1
" R commands in R output are highlighted
let g:Rout_more_colors = 1

" -----
" Custom statusline
" -----

let g:currentmode={
	\ 'n'  : 'N',
	\ 'no' : 'N·Pending',
	\ 'v'  : 'V',
	\ 'V'  : 'V·Line',
	\ '' : 'V·Block',
	\ 's'  : 'S',
	\ 'S'  : 'S·Line',
	\ '' : 'S·Block',
	\ 'i'  : 'I',
	\ 'R'  : 'R',
	\ 'Rv' : 'V·Replace',
	\ 'c'  : 'C',
	\ 'cv' : 'Vim Ex',
	\ 'ce' : 'Ex',
	\ 'r'  : 'Prompt',
	\ 'rm' : 'More',
	\ 'r?' : 'Confirm',
  \ 't'  : 'T',
	\ '!'  : 'Shell',
	\}

" Automatically change the statusline color depending on mode
function! ChangeModeColor()
  if (mode() =~# '\v(n|no)')
    exe 'hi! User1 ctermfg=242 ctermbg=NONE'
  elseif (mode() =~# '\v(v|V)' || g:currentmode[mode()] ==# 'V·Block' || get(g:currentmode, mode(), '') ==# 't')
    exe 'hi! User1 ctermfg=256 ctermbg=005'
  elseif (mode() ==# 'i')
    exe 'hi! User1 ctermfg=256 ctermbg=004'
  elseif (mode() ==# 'R')
    exe 'hi! User1 ctermfg=256 ctermbg=160'
  else
    exe 'hi! User1 ctermfg=242 ctermbg=NONE'
  endif

  return ''
endfunction

" Statusline objects
set laststatus=2
set statusline=
set statusline+=\ %<%F\ %m\ %w\         " File+path
" set statusline+=%#PmenuSel#
" set statusline+=%#LineNr#
set statusline+=%=
" set statusline+=%#CursorColumn#
" set statusline+=\ %y
" set statusline+=\ %{&fileencoding?&fileencoding:&encoding}
" set statusline+=\[%{&fileformat}\]
set statusline+=%{ChangeModeColor()}    " Changing the statusline color
set statusline+=%1*\ %{g:currentmode[mode()]} " Current mode
set statusline+=\ %2n\                  " buffernr
set statusline+=%3p%%\                  " Percentage 
set statusline+=\ %l:%c " Columns and rows

" statusline color 12 is light blue
hi StatusLine ctermfg=242 ctermbg=NONE cterm=NONE
" hi StatusLineNC                ctermfg=232     ctermbg=NONE     cterm=NONE

" Hide the very bottom line stuff
set nosmd                               " short for 'showmode'
set noru                                " short for 'ruler'
set noshowcmd
set shortmess=at

" Specific colors for statusline %1*
highlight User1 ctermfg=242 ctermbg=NONE
