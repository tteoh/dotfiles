#!/bin/bash

# Most of the images are stored on the mmc, mount if needed
# sudo mount /dev/mmcblk0p1 /mnt/mmc

# Firstrun need to install virtio drivers from the iso to get network stuff working
# qemu-system-x86_64 -enable-kvm -m 1024 -cdrom /mnt/mmc/images/virtio-win-0.1.126.iso -drive file=windows-xp.qcow2,format=qcow2 -boot order=c -net nic,macaddr=52:54:00:00:00:00,model=virtio -net user,smb=/home/tteoh/Downloads/shared/

# Virtio drivers
# Install the guest additions, and the ethernet driver (KVM)

# Install Office
qemu-system-x86_64 -enable-kvm -m 1024 -cdrom /mnt/mmc/images/office2010-professionalplus.iso -drive file=windows-xp.qcow2,format=qcow2 -boot order=c -net nic,macaddr=52:54:00:00:00:00,model=virtio -net user,smb=/home/tteoh/Downloads/shared/


# qemu-system-x86_64 -enable-kvm -m 1024 -drive file=windows-xp.qcow2,format=qcow2 -boot order=c -net nic,macaddr=52:54:00:00:00:00,model=virtio -net user,smb=/home/tteoh/Downloads/shared/,smbserver=10.0.2.4
