#!/bin/bash

VNCcmd=$1
VNCport=$2

if [ -z "$VNCcmd" ]; then
  VNCcmd=start
  echo "Defaulting to start server"
fi

if [ -z "$VNCport" ]; then
	VNCport=0
	echo "No port specified, setting VNC port to $VNCport"
fi

echo $VNCcmd

if [ "$VNCcmd" == "start" ]; then
	echo "Starting VNC server on port $VNCport"
  # With tigervnc
	vncserver :$VNCport -geometry 1280x720 -depth 24


  # With tigervnc but serving the X11 server
	# x0vncserver -display=:0 -PlainUsers=tteoh -PasswordFile=.vnc/passwd -FrameRate=30 # -Geometry=400x720
elif [ "$VNCcmd" == "stop" ]; then
	echo "Stopping VNC server on port $VNCport"
	vncserver -kill :$VNCport
else
	echo "Need to specify 'start' or 'stop'"
fi

# 78s9UKajGFxc4rN7
