#!/bin/bash

# Turn off secondary display in sway
# swaymsg output eDP-1 disable

# Turn off power to laptop screen
# sudo vbetool dpms off

# (Re)start bluetooth service
sudo modprobe btusb
